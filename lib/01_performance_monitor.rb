def measure(num=1, &block)
    t0 = Time.now
    num.times {yield}
    (Time.now - t0)/num
end
