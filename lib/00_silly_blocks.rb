def reverser(&block)
    yield.split(' ').map {|word| word.reverse}.join(" ")
end

def adder(num=1, &block)
    num + yield
end

def repeater(num=1, &block)
    num.times {yield}
end
